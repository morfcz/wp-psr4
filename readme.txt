=== PSR-4 for WordPress ===

Contributors: morfcz
Tags: psr-4, autoloading
Donate link: XXX
Requires at least: 4.0
Tested up to: 4.8.3
Requires PHP: PHP 5.3.0
License: GPL 2
License URI: https://gitlab.com/morfcz/wp-psr4/blob/master/LICENSE
Stable Tag: trunk

Provides PSR-4 autoloader class as WP plug-in, that can be easily used by other plug-ins with PSR-4 compatible code-base.

== Description ==

Plug-in provides initialized PSR-4 autoloader class for any plug-in, that may re-use the PSR-4 autoloader without implementing it on its own. See example implementation for more details.

== Installation ==

You just install and enable the plug-in like any other WordPress plug-in.

= Example Implementation =

`<?php

// after plug-ins are loaded
add_action('plugins_loaded', function() {
    // get global instance of psr-4 autoloading class
    global $wpPsr4;

    if (!$wpPsr4 instanceof \WpPsr4\Psr4) {
        add_action('admin_notices', function() {
            printf(
                '<div class="notice notice-warning"><p>%s</p></div>',
                __(
                    'Plugin "PSR-4 for WordPress Example Implementation" cannot be initialized, because it requires "PSR-4 for WordPress" plug-in to be active.',
                    'wp-psr4-example'
                )
            );
        });

        return;
    }

    // add your namespace
    $wpPsr4->addNamespace('WpPsr4Example', __DIR__ . '/src/WpPsr4Example');
    // initiate your plugin
    WpPsr4Example\Plugin::init();
});`

== Changelog ==

- 1.0.1: Tested version
- 1.0.0: Initial version

== GIT Repository ==

- [git@gitlab.com:morfcz/wp-psr4.git](ssh://git@gitlab.com:morfcz/wp-psr4.git)

== PSR-4 Standard Documentation ==

- [PSR-4 Standard](http://www.php-fig.org/psr/psr-4/)
- [Examples of autoloader](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md)
- Check `src/Example` for the same examples
