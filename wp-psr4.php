<?php
/**
Plugin Name: PSR-4 for WordPress
Plugin URI: https://gitlab.com/morfcz/wp-psr4
Description: Creates PSR-4 autoloading plug-in and class for easy support of plug-ins with PSR-4 compatible source code.
Version: 1.0.1
Author: morfcz
Author URI: https://gitlab.com/morfcz/
*/

// Require PSR-4 autoloading class
require_once 'src/WpPsr4/Psr4.php';

WpPsr4\Psr4::init();
