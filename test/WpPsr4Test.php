<?php

use \PHPUnit\Framework\TestCase;

class WpPsr4Test extends TestCase
{
    protected $loader;

    public function testWpPsr4()
    {
        if (HAS_WP) {
            global $wpPsr4;

            $this->assertInstanceOf(\WpPsr4\Psr4::class, $wpPsr4);
        }
    }
}
