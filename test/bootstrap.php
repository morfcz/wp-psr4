<?php

// setup php
error_reporting(E_ALL);
ini_set('display_errors', true);

// define constants
define('WP_USE_THEMES', false);
define('WP_PSR4_DIR_PLUGIN', dirname(__DIR__) . '/');
define('WP_PSR4_DIR_WP', dirname(dirname(dirname(WP_PSR4_DIR_PLUGIN))) . '/');

define('HAS_WP', is_file(WP_PSR4_DIR_WP . 'wp-blog-header.php'));

// if we have wordpress, we will bootstrap it, otherwise we will include only tested class
if (HAS_WP) {
    require WP_PSR4_DIR_WP . 'wp-blog-header.php';
} else {
    require_once WP_PSR4_DIR_PLUGIN . 'src/WpPsr4/Psr4.php';
}

// add testing mock object
require_once WP_PSR4_DIR_PLUGIN . 'test/Mock/Psr4.php';
